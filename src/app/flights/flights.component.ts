import { Component, OnInit } from '@angular/core';
import { FlightService } from './flights.service';

@Component({
  selector: 'app-flights',
  templateUrl: './flights.component.html',
  styleUrls: ['./flights.component.css']
})
export class FlightsComponent implements OnInit {

  todaysDate = new Date();
  tomorrowsDate = new Date(new Date().setDate(new Date().getDate() + 1));

  flights: any;

  length = 0;

  pageSize = 10;

  isLoading = true;

  searchText = '';

  inputValid = true;

  constructor(
    private flightService: FlightService
  ) { }

  ngOnInit() {
    this.flightService.getFlights(this.todaysDate, this.tomorrowsDate)
      .subscribe(flights => {
        this.length = flights.page.fullCount;
        this.flights = flights.operationalFlights;
        this.isLoading = false;
      });
  }

  getFlightsByPage(event: any) {
    this.isLoading = true;
    this.flightService.getFlights(this.todaysDate, this.tomorrowsDate, event.pageIndex + 1)
      .subscribe(flights => {
        this.length = flights.page.fullCount;
        this.flights = flights.operationalFlights;
        this.isLoading = false;
      });
  }

  inputFlightNum(input: string) {
    this.searchText = input;
  }

  search() {
    this.inputValid = true;
    // Need to match flight code: 01 ABCD.
    if (
      !(this.searchText.charAt(0).match(/[a-z]/i) &&
      this.searchText.charAt(1).match(/[a-z]/i) &&
      this.searchText.charAt(2).match(/[0-9]/i) &&
      this.searchText.charAt(3).match(/[0-9]/i) &&
      this.searchText.charAt(4).match(/[0-9]/i) &&
      (this.searchText.charAt(5).match(/[0-9]/i)) || this.searchText.length === 5)
    ) {
      this.inputValid = false;
      return;
    }

    this.isLoading = true;
    this.flightService
      .getFlights(this.todaysDate, this.tomorrowsDate, void 0, Number(this.searchText.slice(2, 6)), this.searchText.slice(0, 2))
      .subscribe(flights => {
        this.length = flights.page.fullCount;
        this.flights = flights.operationalFlights;
        this.isLoading = false;
      });
  }
}
