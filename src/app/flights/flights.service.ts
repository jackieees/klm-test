import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';


@Injectable({
    providedIn: 'root'
})
export class FlightService {

httpOptions = {
  headers: new HttpHeaders({
    Accept: 'application/hal+json',
    'Api-Key': '563bwsemz4wzawscs792yy78',
    'Accepted-Language': 'en-US'
  })
};

constructor(
  private http: HttpClient
) { }

  /** Get flight with stats. */
  getFlights(departure: Date, arrival: Date, pageNumber = 1, flightNumber?: number, carrierCode?: string): Observable<any> {
    let flightSearch = '';
    if (flightNumber && carrierCode) {
      flightSearch = '&carrierCode=' + carrierCode; // TODO add flightnumber + '&flightNumber=' + flightNumber; [Jackie]
    }
    return this.http.get<any>(
      'https://api.airfranceklm.com/opendata/flightstatus/' +
      '?origin=AMS&startRange=' + departure.toISOString()
        .substring(0, departure.toISOString().length - 5) + 'Z' +
        '&endRange=' + arrival.toISOString().substring(0, arrival.toISOString().length - 5) + 'Z' +
        '&pageSize=10&pageNumber=' + pageNumber + flightSearch,
      this.httpOptions);
  }
}
